import {BrowserRouter, Route, Routes} from "react-router-dom";
import {Provider} from "react-redux";
import store from "../store";
import {UsersPage} from "./UsersPage";
import WelcomePage from "./WelcomePage";
import UserDetailPage from "./UserDetail";
import AboutUsPage from "./AboutUsPage";
import Header from "./Header";

const App = () => {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Header/>
                <Routes>
                    <Route path="/user/:id" element={<UserDetailPage/>}/>
                    <Route path="/user" element={<UsersPage/>}/>
                    <Route path="/about-us" element={<AboutUsPage/>}/>
                    <Route path="/" element={<WelcomePage/>}/>
                </Routes>
                <footer>
                    <svg className='footer-background' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                        <path fill="#f3f4f5" fillOpacity="1"
                              d="M0,64L48,53.3C96,43,192,21,288,58.7C384,96,480,192,576,240C672,288,768,288,864,277.3C960,267,1056,245,1152,208C1248,171,1344,117,1392,90.7L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
                    </svg>
                </footer>
            </BrowserRouter>
        </Provider>
    );
};

export default App;
