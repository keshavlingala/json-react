import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {limitChange, offsetChange, photosChange, titleChange} from "../actionCreators/actions";
import {Link} from "react-router-dom";

export function UsersPage() {
    const storedPhotos = useSelector(state => state.photos);
    const [photos, setPhotos] = useState(storedPhotos);
    const dispatch = useDispatch();
    const [offset, setOffset] = useState(useSelector(state => state.offset));
    const [limit, setLimit] = useState(useSelector(state => state.limit));
    useEffect(() => {
        void fetchUsers()
        dispatch(titleChange("Users"));
        dispatch(offsetChange(offset));
        dispatch(limitChange(limit));
    }, [offset, limit])

    async function fetchUsers(force = false) {
        let photos;
        if (storedPhotos.length > 0 && !force) {
            photos = storedPhotos;
        } else {
            const response = await fetch('https://jsonplaceholder.typicode.com/photos');
            photos = await response.json();
        }
        setPhotos(photos)
        dispatch(photosChange(photos));
        return photos;
    }

    return (
        <div className='container'>
            <label htmlFor="limit">Limit: </label>
            <input id='limit' type="number" value={limit} onChange={(e) => setLimit(+e.target.value)}/>

            <div className="table">
                {photos.slice(offset, offset + limit).map((photo) => {
                    return (
                        <div key={photo.id} className="table-row">
                            <div className="table-cell">{photo.id}</div>
                            <div className="table-cell">{photo.title}</div>
                            <div className="table-cell">
                                <img width='75px' height='75px' src={photo.thumbnailUrl} alt="Img"/>
                            </div>
                            <div className="table-cell">
                                <Link to={`/user/${photo.id}`}>
                                    <button>Open</button>
                                </Link>
                            </div>
                        </div>
                    )
                })}
            </div>
            <div className="pagination">
                <button onClick={() => setOffset(offset - limit)} disabled={offset === 0}>Prev</button>
                <button onClick={() => void fetchUsers(true)}>Refresh</button>
                <button onClick={() => setOffset(offset + limit)} disabled={offset + limit >= photos.length}>Next
                </button>
            </div>
        </div>
    );
}
