import {Link, useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {photoChange, titleChange} from "../actionCreators/actions";

export default function UserDetailPage() {
    const {id} = useParams();
    const storePhoto = useSelector(state => state.photos.find(photo => photo.id === +id))
    const [photo, setPhoto] = useState(storePhoto);
    const dispatch = useDispatch();

    async function fetchPhoto(force = false) {
        let photo;
        if (storePhoto && !force) {
            photo = storePhoto;
        } else {
            const response = await fetch(`https://jsonplaceholder.typicode.com/photos/${id}`);
            photo = await response.json();
        }
        setPhoto(photo);
        dispatch(photoChange(photo));
        return photo;
    }

    useEffect(() => {
        void fetchPhoto();
        dispatch(titleChange("User detail"));
    }, [id])
    return (
        <div className='container'>
            {photo && (
                <div className='big-box'>
                    <a href={photo.thumbnailUrl} target='_blank' rel="noreferrer">
                        <img width='250px'
                             src={photo.thumbnailUrl}
                             alt="Big Img"/>
                    </a>
                    <h1>{photo.title}</h1>
                    <h2>{photo.id}</h2>
                </div>
            ) || <h1>Loading...</h1>}
            <div className='pagination'>
                <Link to={`/user/${+id - 1}`}>
                    <button>Prev</button>
                </Link>
                <button onClick={() => void fetchPhoto(true)}>Refresh</button>
                <Link to={`/user/${+id + 1}`}>
                    <button>Next</button>
                </Link>
            </div>
        </div>
    );
}
