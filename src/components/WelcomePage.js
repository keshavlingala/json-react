import {Link} from "react-router-dom";
import {useEffect} from "react";
import {useDispatch} from "react-redux";
import {titleChange} from "../actionCreators/actions";

export default function WelcomePage() {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(titleChange('Home'));
    }, [])
    return (
        <div style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            textAlign: 'center',
        }}>
            <div>
                <h1>Welcome</h1>
                <h2>These are available pages </h2>
                <div>
                    <Link to={'/user'}>
                        <button>Users page</button>
                    </Link>
                    <Link to={'/about-us'}>
                        <button>About Us</button>
                    </Link>
                </div>
            </div>
        </div>
    );
}
