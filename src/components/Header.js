import {Link} from "react-router-dom";
import GhostSVG from "../images/ghost.svg";
import {useSelector} from "react-redux";

export default function Header() {
    const title = useSelector(state => state.title);
    return (
        <header>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
            }
            }>
                <Link to={'/'}><img src={GhostSVG} alt=""/></Link>
                <h1 className='header-title'>{title}</h1>
            </div>
            <div>
                <Link to="/">Home</Link>
                <Link to="/about-us">About Us</Link>
                <Link to="/user">Users</Link>
            </div>
        </header>
    );
}
