export default function titleReducer(state = "", action) {
  switch (action.type) {
    case "CHANGE_TITLE":
      return action.payload;
    default:
      return state;
  }
}
