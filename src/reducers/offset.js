export default function offsetReducer(state = 0, action) {
    switch (action.type) {
        case "CHANGE_OFFSET":
            return action.payload;
        default:
            return state;
    }
}
