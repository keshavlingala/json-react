import {combineReducers} from "redux";
import titleReducer from "./title";
import photosReducer from "./photos";
import offsetReducer from "./offset";
import limitReducer from "./limit";

export default combineReducers({
    photos: photosReducer,
    title: titleReducer,
    offset: offsetReducer,
    limit: limitReducer
});
