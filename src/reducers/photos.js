export default function photosReducer(state = [], action) {
    switch (action.type) {
        case "CHANGE_PHOTOS":
            return action.payload;
        case "CHANGE_PHOTO":
            state.push(action.payload)
            return state;
        default:
            return state;
    }
}
