export default function limitReducer(state = 10, action) {
    switch (action.type) {
        case "CHANGE_LIMIT":
            return action.payload;
        default:
            return state;
    }
}
