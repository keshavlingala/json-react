export function titleChange(title) {
    return { type: "CHANGE_TITLE", payload: title };
}
export function photosChange(photos) {
    return { type: "CHANGE_PHOTOS", payload: photos };
}
export function photoChange(photo) {
    return { type: "CHANGE_PHOTO", payload: photo };
}
export function limitChange(limit) {
    return { type: "CHANGE_LIMIT", payload: limit };
}
export function offsetChange(offset) {
    return { type: "CHANGE_OFFSET", payload: offset };
}
